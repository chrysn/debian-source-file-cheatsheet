Debian source packages come with all kinds of files in the `debian/` directory,
and it is not always trivial to find out what a file in there actually does.

This project aims to document those files / provide a mapping from the files to
the tools that then document it. The current approach is to create a web page
usable for lookup, but I'm open to different approaches of making the
information available.
